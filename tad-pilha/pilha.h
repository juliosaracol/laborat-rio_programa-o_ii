struct descritorPilha{
		int quantidade;
		int quantidadeMaxima;
		struct infoAlunos *topo;	
}

struct infoAlunos{
	int matricula;
	char nome[50];
	float notas[3];
	struct infoAlunos *ptr;
};

struct descritorPilha* inicilizaPilha(int max);
struct infoAlunos* inicializaAluno(void);
void pop(struct descritorPilha *minhaPilha);
void push(struct descritorPilha *minhaPilha, struct infoAlunos *novoElemento);
void busca(struct descritorPilha *minhaPilha, int chave);
void top(struct descritorPilha *minhaPilha);
void tamanho(struct descritorPilha *minhaPilha);
