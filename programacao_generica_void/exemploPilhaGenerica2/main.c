#include <stdio.h>
#include <stdlib.h>
#include "pilhaGenericaAlunosProf.h"

int main()
{
struct descritorPilha *minhaPilha;
minhaPilha = criaPilha();
int op,d,matricula,siape;
char nome[50];
do{
	printf("========================MENU==================================\n");
	printf("1-inserir um novo individuo a lista,\n2-mostrar pilha todos,\n3-mostrar lista de professores e alunos cadastrados separadamente,\n4-excluir um individuo da lista,\n0-sair:\n");
	printf("==========================================================\n");
	setbuf(stdin,NULL);
	scanf("%d",&op);
	if(op==1){
		printf("1-inserir um aluno,2-inserir um professor\n");
		scanf("%d",&d);
		if(d==1){
			printf("digite o nome do aluno:\n");
			setbuf(stdin,NULL);
			scanf("%[^\n]s",nome);
			printf("digite a matricula do aluno:\n");
			setbuf(stdin,NULL);
			scanf("%d",&matricula);
			minhaPilha = criaAluno(nome,matricula,minhaPilha);
		}
		if(d==2){
			printf("digite o nome do professor:\n");
			setbuf(stdin,NULL);
			scanf("%[^\n]s",nome);
			printf("digite o numero de cadastro do professor:\n");
			setbuf(stdin,NULL);
			scanf("%d",&siape);
			minhaPilha = criaProfessor(nome,siape,minhaPilha);
		}
	}
	if(op==2){
		printf("IMPRIMINDO TUDO:\n");
		percorrePilha(minhaPilha,imprime);
    }
	if(op==3){
	    printf("IMPRIMINDO ALUONS:\n");
		percorrePilhaRestricao(minhaPilha,imprimeEspecial, 1);
	    printf("IMPRIMINDO PROFESSORES:\n");
		percorrePilhaRestricao(minhaPilha,imprimeEspecial, 2);
	}
	if(op==4){
	    printf("NAO IMPLEMENTADO AINDA\n");
	}
}while(op!=0);

    return 0;
}
