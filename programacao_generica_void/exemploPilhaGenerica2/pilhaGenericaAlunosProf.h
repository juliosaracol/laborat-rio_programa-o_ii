//------------descrição da pilha-------------------//
enum tipoNodo {aluno=1,professor=2};

struct descritorPilha{
int quantidade;
struct nodo *topo;
};

struct nodo {
void *info;
struct nodo *prox;
//para descobrir qual tipo do nodo
enum tipoNodo tipo;
};
//------------operações da pilha-------------------//
struct descritorPilha * criaPilha(void);
struct nodo* criaNodo(void *info, enum tipoNodo tipo);
struct descritorPilha* insereNodo(struct descritorPilha *pilha, struct nodo *novo);


//uso de callback
void percorrePilha(struct descritorPilha *minhaPilha, void (*callback)(void *, enum tipoNodo));

void percorrePilhaRestricao(struct descritorPilha *minhaPilha, void (*callback)(void *,enum tipoNodo,enum tipoNodo), enum tipoNodo restricao);

//-------------------------------------------------//



//------------------------------------------------//
//---------------operações e definição sobre alunos e professores-----
struct infoAluno{
	int matricula;
	char nome[250];
};
struct infoProf{
	int siape;
	char nome[250];
};


//struct infoAluno* criaAluno(char *nome, int matricula);
struct descritorPilha* criaAluno(char *nome, int matricula, struct descritorPilha *pilha);

//struct infoProf* criaProfessor(char *nome, int siepe);
struct descritorPilha* criaProfessor(char *nome, int siape, struct descritorPilha *pilha);

//usada na funcao de callback
void imprime(void *info, enum tipoNodo tipo);

void imprimeEspecial(void *info, enum tipoNodo tipo, enum tipoNodo tipoBuscado);

