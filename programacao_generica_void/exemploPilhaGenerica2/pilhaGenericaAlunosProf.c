#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilhaGenericaAlunosProf.h"
struct descritorPilha * criaPilha(void){
	struct descritorPilha *novaPilha = (struct descritorPilha *) malloc(sizeof(struct  descritorPilha));
	novaPilha->topo = NULL;
	novaPilha->quantidade=0;
	return novaPilha;
}

struct nodo* criaNodo(void *dadoInfo, enum tipoNodo tipo){
	struct nodo *novo =(struct nodo*)malloc(sizeof(struct nodo));
	novo->prox = NULL;
	novo->info = dadoInfo;
	novo->tipo = tipo;
	return novo;
}


struct descritorPilha* insereNodo(struct descritorPilha *pilha, struct nodo *novo){
	pilha->quantidade++;
	novo->prox = pilha->topo;
	pilha->topo = novo;
	return pilha;
}


//viabiliza chamada de callback
void percorrePilha(struct descritorPilha *minhaPilha, void (*callback)(void *,enum tipoNodo)){
	struct nodo* aux = minhaPilha->topo;
	while(aux != NULL){
		printf("laço percorre pilha\n");
		callback(aux->info,aux->tipo);
		aux = aux->prox;
	}

}


//viabiliza chamada de callback
void percorrePilhaRestricao(struct descritorPilha *minhaPilha, void (*callback)(void *,enum tipoNodo,enum tipoNodo), enum tipoNodo restricao){
	struct nodo* aux = minhaPilha->topo;
	while(aux != NULL){
		printf("laço percorre pilha\n");
		callback(aux->info,aux->tipo, restricao);
		aux = aux->prox;
	}

}


//--------------------operações sobre a definição dos dados-----------------------/
//struct infoAluno* criaAluno(char *nome, int matricula){
struct descritorPilha* criaAluno(char *nome, int matricula, struct descritorPilha *pilha){
	struct infoAluno *novoAluno = (struct infoAluno *)malloc(sizeof(struct infoAluno));
	novoAluno->matricula  = matricula;
	strcpy(novoAluno->nome,nome);
	return insereNodo(pilha,criaNodo(novoAluno,1));
}

//struct infoProf* criaProfessor(char *nome, int siepe){
struct descritorPilha* criaProfessor(char *nome, int siape, struct descritorPilha *pilha){
	struct infoProf *novoProf = (struct infoProf *)malloc(sizeof(struct  infoProf));
	novoProf->siape  = siape;
	strcpy(novoProf->nome,nome);
	return insereNodo(pilha,criaNodo(novoProf,2));
}

void imprime(void *info, enum tipoNodo tipo){
	if(tipo == 1){ //aluno
		printf("======aluno==========\n");
		struct infoAluno *aluno = (struct infoAluno *) info;
		printf(" aluno nome: %s, com matricula %d\n", aluno->nome, aluno->matricula);
	}
	if(tipo == 2){ //professor
		struct infoProf *prof = (struct infoProf *) info;
		printf("=======professor=======\n");
		printf(" professor nome: %s, com siape %d\n", prof->nome, prof->siape);
	}	
}

void imprimeEspecial(void *info, enum tipoNodo tipo, enum tipoNodo tipoBuscado){
	switch(tipoBuscado){
	case 1:
			printf("======aluno==========\n");
			if(tipo == tipoBuscado){
				struct infoAluno *aluno = (struct infoAluno *) info;
				printf("nome: %s, com matricula/siape %d\n", aluno->nome, aluno->matricula);
			}
			break;			
	case 2:
			printf("=======professor=======\n");
			if(tipo == tipoBuscado){
				struct infoProf *prof = (struct infoProf *) info;
				printf("nome: %s, com siape %d\n", prof->nome, prof->siape);
			}
			break;			
	}
}

