#include<stdio.h>
void my_function() {
   printf("This is a normal function.\n");
}


int my_function2(int *soma) {
   printf("This is a normal function 2.\n");
   return 0;
}


void my_callback_function(void (*ptr)()) {
   printf("This is callback function.\n");
   (*ptr)();   //calling the callback function
}

int main() {
   void (*ptr)() = &my_function;
   my_callback_function(ptr);
   //ou
   my_callback_function(my_function2);
   
   return 0;
}
