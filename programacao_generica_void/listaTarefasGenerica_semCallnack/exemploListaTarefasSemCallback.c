#include <stdlib.h>
#include "tarefas.h"

int main(void) {

	Pessoa* julio = pessoa_create("Julio");	
	adicionar_tarefa(julio, tarefa("Publicar artigo sobre listas genericas em C", 0));
	adicionar_tarefa(julio, tarefa("Escrever sobre games em SDL e C++", 1));
	adicionar_tarefa(julio, tarefa("Concluir o curso de microeletronica", 0));
	adicionar_tarefa(julio, tarefa("Escrever um artigo sobre Arduino", 1));
	adicionar_telefone(julio, telefone(TEL_CELULAR, "11", "90000-0000"));
	adicionar_telefone(julio, telefone(TEL_FIXO, "11", "0000-0000"));
	listar_detalhes(julio);
	pessoa_destroy(julio);
	return 0;
}
