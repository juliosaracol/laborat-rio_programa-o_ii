#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifndef PILHA_H
#define PILHA_H
#define TAM_MAX 10

typedef struct {
    void* itens[TAM_MAX];
    int tamItem;
    int topo;
} Pilha;

#endif

#define TRUE 1
#define FALSE 0

//Pilha* criarPilha();
Pilha* criarPilha(int tamItem);

void liberarPilha(Pilha* pilha);
int estahVazia(Pilha* pilha);
int estahCheia(Pilha* pilha);

//void empilhar(int item, Pilha* pilha);
void empilhar(void *item, Pilha* pilha);

//int desempilhar(Pilha* pilha);
void* desempilhar(Pilha* pilha);

//int obterTopo(Pilha* pilha);
void* obterTopo(Pilha* pilha);
