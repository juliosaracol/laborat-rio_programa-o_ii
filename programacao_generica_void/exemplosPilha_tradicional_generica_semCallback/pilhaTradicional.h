#include <stdio.h>
#include <stdlib.h>


#ifndef PILHA_H
#define PILHA_H
#define TAM_MAX 10

typedef struct {
    int itens[TAM_MAX];
    int topo;
} Pilha;

#endif

#define TRUE 1
#define FALSE 0

Pilha* criarPilha();
void liberarPilha(Pilha* pilha);
int estahVazia(Pilha* pilha);
int estahCheia(Pilha* pilha);
void empilhar(int item, Pilha* pilha);
int desempilhar(Pilha* pilha);
int obterTopo(Pilha* pilha);
void imprimirPilha(Pilha* pilha);
