#include "pilhaTradicional.h"

int main() {
    Pilha* minhaPilha = criarPilha();
    empilhar(1, minhaPilha); 
    empilhar(2, minhaPilha); 
    empilhar(3, minhaPilha);
    desempilhar(minhaPilha); 
    empilhar(4, minhaPilha); 
    imprimirPilha(minhaPilha);
    return 0;
}
