#include "pilhaGenerica.h"


//----------------------------------------------------------------
//------INTEIROS--------------------------------
typedef struct {
    int info;
} ItemInt;

void imprimirPilhaInt(Pilha* pilha) {
     while(!estahVazia(pilha)) 
		printf("%d\n", ((ItemInt*)desempilhar(pilha))->info);
}

void adicionarElementoInt(int v, Pilha* p) {
    ItemInt* i = (ItemInt*) malloc(sizeof(ItemInt));
    i->info = v; 
    empilhar(i, p);
}

//------STRING--------------------------------

typedef struct {
    char info[255];
} ItemStr;

void imprimirPilhaStr(Pilha* pilha) {
    while(!estahVazia(pilha)) 
		printf("%s\n",((ItemStr*)desempilhar(pilha))->info);
}

void adicionarElementoStr(char s[], Pilha* p) {
    ItemStr* i = (ItemStr*) malloc(sizeof(ItemStr));
    strcpy(i->info, s); 
    empilhar(i, p);
}

//---------------------------------------------------------------//

int main() {
    Pilha* minhaPilhaInt = criarPilha(sizeof(ItemInt));

    adicionarElementoInt(1, minhaPilhaInt);
    ItemInt *no = (ItemInt *) obterTopo(minhaPilhaInt); 
    printf("topo  %d\n", (int) (no->info));
    adicionarElementoInt(2, minhaPilhaInt);
    no = (ItemInt *)obterTopo(minhaPilhaInt); 
    printf("topo  %d\n", (int) (no->info));
    adicionarElementoInt(3, minhaPilhaInt); 
    no = (ItemInt *)obterTopo(minhaPilhaInt); 
    printf("topo  %d\n", (int) (no->info));
    desempilhar(minhaPilhaInt);
    no = (ItemInt *)obterTopo(minhaPilhaInt); 
    printf("topo  %d\n", (int) (no->info));
    adicionarElementoInt(4, minhaPilhaInt);
    no = (ItemInt *)obterTopo(minhaPilhaInt); 
    printf("topo  %d\n", (int) (no->info));
	imprimirPilhaInt(minhaPilhaInt);

    printf("\n\n");

    Pilha* minhaPilhaStr = criarPilha(sizeof(ItemStr));
    adicionarElementoStr("A", minhaPilhaStr); 
    ItemStr *noS = obterTopo(minhaPilhaStr);
    printf("topo  %s\n", (char *) (noS->info));
    adicionarElementoStr("B", minhaPilhaStr);
    noS = obterTopo(minhaPilhaStr);
    printf("topo  %s\n", (char *) (noS->info));
    adicionarElementoStr("C", minhaPilhaStr); 
    noS = obterTopo(minhaPilhaStr);
    printf("topo  %s\n", (char *) (noS->info));
    desempilhar(minhaPilhaStr);
	noS = obterTopo(minhaPilhaStr);	
    printf("topo  %s\n", (char *) (noS->info));
    adicionarElementoStr("D", minhaPilhaStr);
    noS = obterTopo(minhaPilhaStr);
    printf("topo  %s\n", (char *) (noS->info));
	imprimirPilhaStr(minhaPilhaStr);
    return 0;
}
