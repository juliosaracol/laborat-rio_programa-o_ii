#include "pilhaTradicional.h"

Pilha* criarPilha() {
    Pilha* pilha = (Pilha*) malloc(sizeof(Pilha));
    pilha->topo = 0;
    return pilha;
}

void liberarPilha(Pilha* pilha) {
    if (pilha != NULL) free(pilha);
}

int estahCheia(Pilha* pilha) {
    if (pilha->topo == TAM_MAX) return TRUE;
    else return FALSE;
}

int estahVazia(Pilha* pilha) {
    if (pilha->topo == 0) return TRUE;
    else return FALSE;
}

void empilhar(int item, Pilha* pilha) {
    if (!estahCheia(pilha)) {
        pilha->itens[pilha->topo] = item;
        pilha->topo++;
    } else printf("Pilha cheia!\n");
}

int desempilhar(Pilha* pilha) {
    if (!estahVazia(pilha)) {
        int elementoTopo = pilha->itens[pilha->topo - 1];
        pilha->topo--;
        return elementoTopo;
    } else printf("Pilha vazia!\n");
}

int obterTopo(Pilha* pilha) {
    if (!estahVazia(pilha)) {
        int elementoTopo = pilha->itens[pilha->topo - 1];
        return elementoTopo;
    } else printf("Pilha vazia!\n");
}


void imprimirPilha(Pilha* pilha) {
    while(!estahVazia(pilha)) printf("%d\n", desempilhar(pilha));
}
