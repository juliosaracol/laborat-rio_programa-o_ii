#include "pilhaGenerica.h"

/*Pilha* criarPilha() {
    Pilha* pilha = (Pilha*) malloc(sizeof(Pilha));
    pilha->topo = 0;
    return pilha;
}
*/

Pilha* criarPilha(int tamItem) {
    Pilha* pilha = (Pilha*) malloc(sizeof(Pilha));
    pilha->topo = 0;
    pilha->tamItem = tamItem; //informa o tamanho (quantidade de bytes) que é utilizado para o elemento
    return pilha;
}


void liberarPilha(Pilha* pilha) {
    if (pilha != NULL) free(pilha);
}

int estahCheia(Pilha* pilha) {
    if (pilha->topo == TAM_MAX) 
		return TRUE;
    else 
		return FALSE;
}

int estahVazia(Pilha* pilha) {
    if (pilha->topo == 0) 
		return TRUE;
    else 
		return FALSE;
}

/*void empilhar(int item, Pilha* pilha) {
    if (!estahCheia(pilha)) {
        pilha->itens[pilha->topo] = &item;
        pilha->topo++;
    } else printf("Pilha cheia!\n");
}
*/
void empilhar(void *item, Pilha* pilha) {
    if (!estahCheia(pilha)) {
        pilha->itens[pilha->topo] = item;
        pilha->topo++;
        printf(" empilhou elemento\n ");
    } 
    else 
		printf("Pilha cheia!\n");
}

/*
 * int desempilhar(Pilha* pilha) {
    if (!estahVazia(pilha)) {
        int elementoTopo = pilha->itens[pilha->topo - 1];
        pilha->topo--;
        return elementoTopo;
    } else printf("Pilha vazia!\n");
}*/
void* desempilhar(Pilha* pilha) {
    if (!estahVazia(pilha)) {
        void* elRemovido = malloc(pilha->tamItem);
        memcpy(elRemovido, pilha->itens[pilha->topo - 1], pilha->tamItem);//copia o conteudo do elemento para retornar 
        pilha->topo--;
        printf("desempilhou\n");
        return elRemovido;
    } 
    else 
		return NULL;
}



/*int obterTopo(Pilha* pilha) {
    if (!estahVazia(pilha)) {
        int elementoTopo = pilha->itens[pilha->topo - 1];
        return elementoTopo;
    } else printf("Pilha vazia!\n");
}
* */
void* obterTopo(Pilha* pilha) {
    if (!estahVazia(pilha)) {
        void* elTopo = malloc(pilha->tamItem);
        memcpy(elTopo, pilha->itens[pilha->topo - 1], pilha->tamItem); //copia o conteudo do elemento para retornar 
        return elTopo;
    } 
    else return NULL;
}




