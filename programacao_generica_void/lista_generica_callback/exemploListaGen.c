#include "exemploListaGen.h"


ListaGen* lgen_insere (ListaGen* L, void* p) {
	ListaGen* n = (ListaGen*) malloc(sizeof(ListaGen));
	n->info = p;
	n->prox = L;
	return n;
}


void lgen_percorre (ListaGen* L, void (*cb)(void*)){
	ListaGen* p;
	for (p=L; p!=NULL; p=p->prox) {
		cb(p->info);
	}
}


ListaGen* insere_ponto (ListaGen* L, float x, float y) {
	Ponto* p = (Ponto*) malloc(sizeof(Ponto));
	p->x = x;
	p->y = y;
	return lgen_insere(L,p);
}


int lgen_percorre2 (ListaGen* L, int (*cb)(void*,void*), void* dado){
	ListaGen* p;
	for (p=L; p!=NULL; p=p->prox) {
		int r = cb(p->info,dado);
		//if (r != 0)
		//	return r;
	}
	return 0;
}


void imprime (void* info){
	printf("funcao imprime\n");
	Ponto* p = (Ponto*)info;
	printf("%f %f\n", p->x, p->y);
}


int igualdade (void* info, void* dado){
	Ponto* p = (Ponto*)info;
	Ponto* q = (Ponto*)dado;
	if ((((p->x) - (q->x)) == 0) && (((p->y) - (q->y)) == 0))
		return 1;
	else
		return 0;
}

int pertence (ListaGen* L, float x, float y){
	Ponto q;
	q.x = x;
	q.y = y;
	return lgen_percorre2(L, igualdade,&q);
}


int centro_geom (void* info, void* dado){
	Ponto* p = (Ponto*)info;
	Cg* cg = (Cg*)dado;
	cg->p.x += p->x;
	cg->p.y += p->y;
	cg->n++;
	return cg->n;
}

