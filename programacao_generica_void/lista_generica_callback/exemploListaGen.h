#include <stdio.h>
#include <stdlib.h>


struct listagen {
	void* info;
	struct listagen* prox;
};
typedef struct listagen ListaGen;

ListaGen* lgen_insere (ListaGen* L, void* p);

//funções adaptadas para receber funções de callback com e sem parametros
void lgen_percorre (ListaGen* L, void (*cb)(void*));
int lgen_percorre2 (ListaGen* L, int (*cb)(void*,void*), void* dado);


//----------------------------------

//modela a representação do ponto
struct ponto {
	float x, y;
};
typedef struct ponto Ponto;


//modela o centro geometrico (calculado a partir dos pontos)
struct cg {
	int n; // número de pontos analisados
	Ponto p; //”soma” dos pontos
};
typedef struct cg Cg;


ListaGen* insere_ponto (ListaGen* L, float x, float y);
void imprime (void* info);
int igualdade (void* info, void* dado);
int pertence (ListaGen* L, float x, float y);
int centro_geom (void* info, void* dado);
