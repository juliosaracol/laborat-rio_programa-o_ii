struct InformacoesAlunos {
 int matricula;
 char nome[50];
 float media;
 struct InformacoesAlunos *prox;
};

struct descritorPilha{
int quantidade;
struct InformacoesAlunos *topo;	
};

struct descritorPilha * criaPilha(void);
struct InformacoesAlunos* criaAlunos(char *nome, float media, int matricula);
struct descritorPilha * push(struct descritorPilha *pilha,struct InformacoesAlunos *novoAluno);
struct descritorPilha  * pop(struct  descritorPilha *pilha);
void top(struct InformacoesAlunos *topo);
