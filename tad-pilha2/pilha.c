#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"

struct descritorPilha * criaPilha(void){
	struct descritorPilha *novaPilha = (struct descritorPilha *) malloc(sizeof(struct  descritorPilha));
	novaPilha->topo = NULL;
	novaPilha->quantidade=0;
	return novaPilha;
}

struct InformacoesAlunos* criaAlunos(char *nome, float media, int matricula){
	struct InformacoesAlunos *novoAluno = (struct InformacoesAlunos *)malloc(sizeof(struct  InformacoesAlunos));
	novoAluno->matricula  = matricula;
	novoAluno->media = media;
	strcpy(novoAluno->nome,nome);
	novoAluno->prox = NULL;
	return novoAluno;
}

struct descritorPilha * push(struct descritorPilha *pilha,struct InformacoesAlunos *novoAluno){
		novoAluno->prox= pilha->topo;
		pilha->topo = novoAluno;
		pilha->quantidade++;
		return pilha;
}

struct descritorPilha  * pop(struct  descritorPilha *pilha){
		struct InformacoesAlunos * temp = pilha->topo;
		pilha->topo = pilha->topo->prox;
		free(temp);
		pilha->quantidade--;
		return pilha;
}


void top(struct InformacoesAlunos *topo){
	if(topo == NULL)
	printf("PILHA VAZIA\n");
	else
	printf("TOPO igual %d %s\n", topo->matricula,topo->nome);
}

