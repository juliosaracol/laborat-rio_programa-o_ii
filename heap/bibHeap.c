//Universidade Federal de Pelotas 
//Julio Saraçol Domingues Júnior 	
//Estrutura de Dados II
//Biblioteca Heap

#include <stdio.h>
#include <stdlib.h>
#include "bibHeap.h"


//funçoes Heap
//função de inserir a heap
int buildHeap (int *heap,int tamanho,int elemento)
{
	tamanho++;
	//realoca um new heap com o novo tamanho
	heap=realloc(heap,tamanho*sizeof(int));
	//completa o novo indice com o valor 
	heap[tamanho-1]=elemento;
	percolate(heap,tamanho);
	return tamanho;
}

//função para deletar o elemento da heap
int deleteHeap(int *heap,int tamanho)
{
	int aux;
	aux=heap[0]; 				//maior elemento 
	heap[0]=heap[tamanho-1];	//menor elemento passa para cabeça da heap
	heap[tamanho-1]=aux;		//maior elemento passa para menor posicao
	tamanho--;
	//realoca um new heap com o novo tamanho
	heap = realloc(heap,tamanho*sizeof(int));
	siftDown(heap,tamanho);		//chama a siftdown para organizar 
	return tamanho;
}

//função para imprimir o estado atual da heap
void printHeap(int *heap,int tamanho)
{
	int i;
	if(tamanho!=0)
	{
		for(i=0;i<tamanho;i++)
		{
			printf("%i -",heap[i]);
		}
	}
	else
		printf("heap vazia\n");
}


//função sift-down
void siftDown(int *heap,int tamanho)
{
	int i,aux=0;	
	for(i=0;i<tamanho-1;i++)
	{
		if(heap[i] < heap[i+1])
		{
			aux=heap[i];
			heap[i]=heap[i+1];
			heap[i+1]=aux;
		}
	}
}
int getMaior(int *heap)
{
	return heap[0];
}


//função percolate
void percolate(int *heap,int tamanho)
{
	int i,aux;
	for(i=tamanho-1;i>0;i--)
	{
		if(heap[i] > heap[i-1])
		{
			aux=heap[i-1];
			heap[i-1]=heap[i];
			heap[i]=aux;
		}
	}
}
