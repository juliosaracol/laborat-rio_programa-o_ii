//Universidade Federal de Pelotas 
//Julio Saraçol Domingues Júnior 	
//Estrutura de Dados II
//Biblioteca Heap

//função que construi uma heap com o elmeento inserido
int buildHeap (int *heap,int tamanho,int elemento);

//função para deletar o elemento da heap
int deleteHeap(int *heap,int tamanho);

//função para imprimir o estado atual da heap
void printHeap(int *heap,int tamanho);

//função sift-down
void siftDown(int *heap,int tamanho);

//função percolate
void percolate(int*heap,int tamanho);

//retorna o maior da heap 
int getMaior(int *heap);
