//Julio Saraçol Domingues Júnior 	
//Biblioteca Heap

#include <stdio.h>
#include <stdlib.h>
#include "bibHeap.h"

main()
{
	char opcao;
	int elemento,*heap,tamanho=0;
	//faz o malloc inicial pq nao pode fazer realloc sem ter área previa definida
	heap=(int *)malloc(tamanho*(sizeof(int)));
	//menu
	printf("::::HEAPS::::\n");
	printf("digite a opcao: i->inserir d->deletar g->get maior p->mostar s->sair heap\n");
	do{
		opcao=getchar();
	 
		switch(opcao)
		{
		
			case 'i':
			{
				printf("digite o valor do elemento a inserir na heap: \n");
				scanf("%i",&elemento);
				tamanho = buildHeap(heap,tamanho,elemento);
				printf("heap final:");
				printHeap(heap,tamanho);
				break;
			}
			case 'd':
			{
				tamanho = deleteHeap(heap,tamanho);
				printf("heap final:");
				printHeap(heap,tamanho);
				break;
			}
			
			case 'g':
			{
				printf("Maior:%i",getMaior(heap));
				break;
			}
			case 'p':
			{
				printf("heap:");
				printHeap(heap,tamanho);
				break;
			}	
			case 's': 	exit(0);break;
		}
		if(opcao != '\n')
				printf("\nDigite sua opcao:\n");
		
	}while(opcao != 's');
}
