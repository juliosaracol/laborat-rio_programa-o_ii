
//biblioteca de conjuntos disjuntos

//funcao para inicializar o vetor dos conjuntos
void inicializar(int * vector, int tamanho);
//funcao para encontrar elementos dos conjuntos
int find(int elemento,int* vector,int tamanho);
//funcao para unir conjuntos
void merge(int elementoA,int elementoB,int *vector,int tamanho);
				
