struct descritor_grafo{
	int max_vertices;
	int max_arestas;
	struct nodo *nodos;
};

struct nodo{
	int chave;
	struct nodo *prox;
	//struct nodo *adjacencias;	
	struct aresta *adjacencias;	
};

struct aresta{
	int partida;
	int chegada;
	int peso;
	struct aresta *prox;
}









struct descritor_grafo* parser(char *nomeArquivo);
struct descritor_grafo * inicializaGrafo(int tamanho);
struct nodo* criaVertice(int chave);
void insereVertice(struct descritor_grafo *grafo, int chave);
struct descritor_grafo * insereAresta(struct descritor_grafo *grafo, int chaveSaida, int chaveChegada);
struct nodo* buscaVertice(struct descritor_grafo *grafo, int chaveBusca);
struct nodo* buscaAdjacencia(struct descritor_grafo *grafo, int partida, int chegada);
int tamanhoVertices(struct descritor_grafo *grafo);
int tamanhoAdjacencias(struct descritor_grafo *grafo);
int listaAjacencias(struct nodo *vertice);
