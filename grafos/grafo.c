#include "grafo.h"
#include <stdlib.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


struct descritor_grafo* parser(char *nomeArquivo){

	FILE *file = fopen(nomeArquivo,"r");
	int total;
	fscanf(file,"%d",&total);
	printf("total de nodos %d\n",total);
	struct descritor_grafo *grafo = NULL;
	grafo = inicializaGrafo(total); 
	char linha[500];
	char caractere;
	int n;
	n=0;
	while((caractere =fgetc(file)) != EOF ){ //le o arquivo até o final caractere por caractere
		if(caractere != '\n'){
			linha[n]=caractere;
			n++;
		}
		else{
			linha[n]='\0';
			if(n > 0 ) //frase valida
			{
			 printf("linha eh: %s\n", linha);
			 int partida = atoi(strtok(linha," "));
			 printf("\n partida strtok= %d\n",partida);
			 int chegada = atoi(strtok(NULL," "));		 
			 printf("\n chegada strtok= %d\n",chegada);
			 grafo = insereAresta(grafo,partida,chegada);


			 //QUEBRA A LINHA NO SEPARADOR E COLOCA AS INFORMAÇÕES NA STRUCT strtok(linha," ")));
			 // LEMBRAR QUE OS PROXIMOS STRTOK O PRIMEIRO PARAMETRO SERÁ NULL
			 //http://www.tutorialspoint.com/c_standard_library/c_function_strtok.htm
			}
			n=0; //zera a frase para pegar proximas informações
			linha[n]='\0';
		}   
	}
	fclose(file);
	return grafo;
}


struct descritor_grafo * inicializaGrafo(int tamanho){
	struct descritor_grafo * novografo = (struct descritor_grafo*) malloc(sizeof(struct descritor_grafo));
	novografo->max_vertices = tamanho;
	novografo->max_arestas = 0;
	novografo->nodos = NULL;
	int i;
	//cria a lista encadeada de nodos
	for(i=0;i<tamanho;i++){
		struct nodo *novoNodo = criaVertice(i+1);
		if(novografo->nodos == NULL){
			novografo->nodos = novoNodo;
		}
		else{
			struct nodo *temp = novografo->nodos;
			while(temp->prox!=NULL){
					temp = temp->prox;
			}
			temp->prox = novoNodo;
		}
	}
	return novografo;
}


struct nodo * criaVertice(int chave){
	struct nodo *novoVertice  = (struct nodo *) malloc(sizeof(struct nodo));
	novoVertice->chave 	= chave;
	novoVertice->prox 	= NULL;
	novoVertice->adjacencias=NULL;
	return novoVertice;
}
 
struct descritor_grafo * insereAresta(struct descritor_grafo *grafo, int chaveSaida, int chaveChegada){
	struct nodo* nodoSaida = buscaVertice(grafo,chaveSaida);
	if(nodoSaida ==NULL){
		printf("nodo buscado não existe\n");
		return grafo;
	}
	struct nodo* nodoChegada = criaVertice(chaveChegada);
	
	if(nodoSaida->adjacencias == NULL)
		nodoSaida->adjacencias=nodoChegada;
	else{
		struct nodo*temp = nodoSaida->adjacencias;
		while(temp->prox != NULL){
			temp = temp->prox;
		}	
		temp->prox = nodoChegada;	
	}
	return grafo;
}


struct nodo * buscaVertice(struct descritor_grafo *grafo, int chaveBusca){
	struct nodo* temporaria = grafo->nodos;
	while(temporaria !=NULL){
		if(temporaria->chave == chaveBusca)
			return temporaria;
		temporaria = temporaria->prox;
	}
	return NULL;	
}


struct nodo * buscaAdjacencia(struct descritor_grafo *grafo, int partida, int chegada);
int tamanhoVertices(struct descritor_grafo *grafo);
int tamanhoAdjacencias(struct descritor_grafo *grafo);
int listaAjacencias(struct nodo *vertice);
